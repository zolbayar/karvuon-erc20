# Karvuon ERC20 Token 


## Development

### Install dependencies

```bash
npm install
```

### Usage (using Truffle)

Open the Truffle console

```bash
npm run console
```

#### Compile

```bash
npm run compile
```

#### Test 

```bash
npm run test 
```

### Usage (using Buidler)

Open the Buidler console

```bash
npm run buidler:console
```

#### Compile

```bash
npm run buidler:compile
```

#### Test 

```bash
npm run buidler:test 
```

### Code Coverage

```bash
npm run coverage
```

## Linter

Use Solhint

```bash
npm run lint:sol
```

Use ESLint

```bash
npm run lint:js
```

Use ESLint and fix

```bash
npm run lint:fix
```

## Flattener

This allow to flatten the code into a single file

Edit `scripts/flat.sh` to add your contracts

```bash
npm run flat
```

## Token verification on Etherscan

Use the dist smart contracts [dist/ERC20Token.dist.sol](https://gitlab.com/zolbayar/karvuon-erc20/tree/master/dist/ERC20Token.dist.sol)

Solc version is 0.5.12


